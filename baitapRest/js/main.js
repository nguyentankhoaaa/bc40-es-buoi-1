
let tinhDiemTB = (...numbers) => {
    let total = 0;
    for(var index = 0;index < numbers.length;index++){
        total += numbers[index];
    }  
    return total / numbers.length;
}

let TinhTbKhoi1 = () => {
    let diemToan = document.getElementById("inpToan").value*1;
    let diemLy = document.getElementById("inpLy").value*1;
    let diemHoa = document.getElementById("inpHoa").value*1;    
    let diemTb = tinhDiemTB(diemLy,diemHoa,diemToan);
    document.getElementById("tbKhoi1").innerText = `${diemTb}`
}

let TinhTbKhoi2 = () => {
    let diemVan = document.getElementById("inpVan").value*1;
    let diemSu = document.getElementById("inpSu").value*1;
    let diemDia = document.getElementById("inpDia").value*1;
    let diemAnhVan = document.getElementById("inpEnglish").value*1;
    let diemTb = tinhDiemTB(diemAnhVan,diemDia,diemSu,diemVan);
    document.getElementById("tbKhoi2").innerHTML=`${diemTb}`;
    }

